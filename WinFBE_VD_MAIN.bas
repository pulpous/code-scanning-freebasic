#Define UNICODE
#Define _WIN32_WINNT &h0602
#Include Once "windows.bi"
#Include Once "Afx\CWindow.inc"
#define CODEGEN_FORM
#define CODEGEN_TEXTBOX
#define CODEGEN_RICHEDIT
#define CODEGEN_BUTTON
#define CODEGEN_LABEL
#Include once "WinFormsX\WinFormsX.bi"
Using Afx

' WINFBE_CODEGEN_START

type frmMainType extends wfxForm
    private:
        temp as byte
    public:
        declare static function FormInitializeComponent( byval pForm as frmMainType ptr ) as LRESULT
        declare constructor
        ' Controls
        Text1 As wfxTextBox
        RichEdit1 As wfxRichEdit
        Button1 As wfxButton
        Label1 As wfxLabel
        Label2 As wfxLabel
end type


function frmMainType.FormInitializeComponent( byval pForm as frmMainType ptr ) as LRESULT
    dim as long nClientOffset

    pForm->Name = "frmMain"
    pForm->Text = "识别系统"
    pForm->StartPosition = FormStartPosition.CenterScreen
    pForm->SetBounds(10,10,527,431)
    pForm->Text1.Parent = pForm
    pForm->Text1.Name = "Text1"
    pForm->Text1.Text = "Text1"
    pForm->Text1.SetBounds(33,39-nClientOffset,454,36)
    pForm->RichEdit1.Parent = pForm
    pForm->RichEdit1.Name = "RichEdit1"
    pForm->RichEdit1.Text = "RichEdit1"
    pForm->RichEdit1.SetBounds(33,150-nClientOffset,449,234)
    pForm->Button1.Parent = pForm
    pForm->Button1.Name = "Button1"
    pForm->Button1.Text = "清除结果列表"
    pForm->Button1.SetBounds(324,105-nClientOffset,150,36)
    pForm->Label1.Parent = pForm
    pForm->Label1.Name = "Label1"
    pForm->Label1.Text = "输入会员id，回车结束"
    pForm->Label1.SetBounds(33,15-nClientOffset,247,16)
    pForm->Label2.Parent = pForm
    pForm->Label2.Name = "Label2"
    pForm->Label2.Text = "结果列表"
    pForm->Label2.SetBounds(33,125-nClientOffset,88,16)
    pForm->Controls.Add(ControlType.TextBox, @(pForm->Text1))
    pForm->Controls.Add(ControlType.RichEdit, @(pForm->RichEdit1))
    pForm->Controls.Add(ControlType.Button, @(pForm->Button1))
    pForm->Controls.Add(ControlType.Label, @(pForm->Label1))
    pForm->Controls.Add(ControlType.Label, @(pForm->Label2))
    Application.Forms.Add(ControlType.Form, pForm)
    function = 0
end function

constructor frmMainType
    InitializeComponent = cast( any ptr, @FormInitializeComponent )
    this.FormInitializeComponent( @this )
end constructor

dim shared frmMain as frmMainType

' WINFBE_CODEGEN_END

' ========================================================================================
' WinFBE - FreeBASIC Editor (Windows 32/64 bit)
' Visual Designer auto generated project
' ========================================================================================

' Main application entry point.
' Place any additional global variables or #include files here.

' For your convenience, below are some of the most commonly used WinFBX library
' include files. Uncomment the files that you wish to use in the project or add
' additional ones. Refer to the WinFBX Framework Help documentation for information
' on how to use the various functions.

' #Include Once "Afx\AfxFile.inc"
 #Include Once "Afx\AfxStr.inc"
' #Include Once "Afx\AfxTime.inc"
' #Include Once "Afx\CIniFile.inc"
' #Include Once "Afx\CMoney.inc"
 #Include Once "Afx\CPrint.inc"


Application.Run(frmMain)

#include once "E:\mayun\pulpous\code-scanning-freebasic\frmMain.inc"

