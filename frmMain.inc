' WINFBE FORM
' WINFBE VERSION 2.2.0
' LOCKCONTROLS=False
' SNAPLINES=True
' WINFBE FORM_START
' WINFBE CONTROL_START Form
'   PROPERTIES_START
'     PROP_NAME=Name
'     PROP_VALUE=frmMain
'     PROP_NAME=Left
'     PROP_VALUE=10
'     PROP_NAME=Top
'     PROP_VALUE=10
'     PROP_NAME=Width
'     PROP_VALUE=527
'     PROP_NAME=Height
'     PROP_VALUE=431
'     PROP_NAME=ChildForm
'     PROP_VALUE=False
'     PROP_NAME=Text
'     PROP_VALUE=form1
'     PROP_NAME=WindowState
'     PROP_VALUE=FormWindowState.Normal
'     PROP_NAME=StartPosition
'     PROP_VALUE=FormStartPosition.CenterScreen
'     PROP_NAME=BorderStyle
'     PROP_VALUE=FormBorderStyle.Sizable
'     PROP_NAME=MinimizeBox
'     PROP_VALUE=True
'     PROP_NAME=MaximizeBox
'     PROP_VALUE=True
'     PROP_NAME=ControlBox
'     PROP_VALUE=True
'     PROP_NAME=Enabled
'     PROP_VALUE=True
'     PROP_NAME=Visible
'     PROP_VALUE=True
'     PROP_NAME=BackColor
'     PROP_VALUE=SYSTEM|Control
'     PROP_NAME=AcceptButton
'     PROP_VALUE=
'     PROP_NAME=AllowDrop
'     PROP_VALUE=False
'     PROP_NAME=KeyPreview
'     PROP_VALUE=False
'     PROP_NAME=CancelButton
'     PROP_VALUE=
'     PROP_NAME=Icon
'     PROP_VALUE=
'     PROP_NAME=Locked
'     PROP_VALUE=False
'     PROP_NAME=MaximumHeight
'     PROP_VALUE=0
'     PROP_NAME=MaximumWidth
'     PROP_VALUE=0
'     PROP_NAME=MinimumHeight
'     PROP_VALUE=0
'     PROP_NAME=MinimumWidth
'     PROP_VALUE=0
'     PROP_NAME=Tag
'     PROP_VALUE=
'   PROPERTIES_END
'   EVENTS_START
'   EVENTS_END
' WINFBE CONTROL_END
' WINFBE CONTROL_START TextBox
'   PROPERTIES_START
'     PROP_NAME=Name
'     PROP_VALUE=Text1
'     PROP_NAME=Left
'     PROP_VALUE=33
'     PROP_NAME=Top
'     PROP_VALUE=39
'     PROP_NAME=Width
'     PROP_VALUE=454
'     PROP_NAME=Height
'     PROP_VALUE=36
'     PROP_NAME=AcceptsReturn
'     PROP_VALUE=False
'     PROP_NAME=AcceptsTab
'     PROP_VALUE=False
'     PROP_NAME=AllowDrop
'     PROP_VALUE=False
'     PROP_NAME=BackColor
'     PROP_VALUE=SYSTEM|Window
'     PROP_NAME=BorderStyle
'     PROP_VALUE=ControlBorderStyle.Fixed3D
'     PROP_NAME=CharacterCasing
'     PROP_VALUE=CharacterCase.Normal
'     PROP_NAME=CueBannerText
'     PROP_VALUE=
'     PROP_NAME=Enabled
'     PROP_VALUE=True
'     PROP_NAME=Font
'     PROP_VALUE=Segoe UI,9,400,0,0,0,1
'     PROP_NAME=ForeColor
'     PROP_VALUE=SYSTEM|WindowText
'     PROP_NAME=HideSelection
'     PROP_VALUE=True
'     PROP_NAME=Locked
'     PROP_VALUE=False
'     PROP_NAME=Multiline
'     PROP_VALUE=False
'     PROP_NAME=PasswordChar
'     PROP_VALUE=
'     PROP_NAME=ReadOnly
'     PROP_VALUE=False
'     PROP_NAME=TextScrollBars
'     PROP_VALUE=ScrollBars.None
'     PROP_NAME=TabIndex
'     PROP_VALUE=1
'     PROP_NAME=TabStop
'     PROP_VALUE=True
'     PROP_NAME=Tag
'     PROP_VALUE=
'     PROP_NAME=ToolTip
'     PROP_VALUE=
'     PROP_NAME=ToolTipBalloon
'     PROP_VALUE=False
'     PROP_NAME=Text
'     PROP_VALUE=Text1
'     PROP_NAME=TextAlign
'     PROP_VALUE=TextAlignment.Left
'     PROP_NAME=Visible
'     PROP_VALUE=True
'     PROP_NAME=WordWrap
'     PROP_VALUE=False
'   PROPERTIES_END
'   EVENTS_START
'   EVENTS_END
' WINFBE CONTROL_END
' WINFBE CONTROL_START RichEdit
'   PROPERTIES_START
'     PROP_NAME=Name
'     PROP_VALUE=RichEdit1
'     PROP_NAME=Left
'     PROP_VALUE=33
'     PROP_NAME=Top
'     PROP_VALUE=150
'     PROP_NAME=Width
'     PROP_VALUE=449
'     PROP_NAME=Height
'     PROP_VALUE=234
'     PROP_NAME=AcceptsReturn
'     PROP_VALUE=True
'     PROP_NAME=AcceptsTab
'     PROP_VALUE=False
'     PROP_NAME=AllowDrop
'     PROP_VALUE=False
'     PROP_NAME=BackColor
'     PROP_VALUE=SYSTEM|Window
'     PROP_NAME=BorderStyle
'     PROP_VALUE=ControlBorderStyle.Fixed3D
'     PROP_NAME=Enabled
'     PROP_VALUE=True
'     PROP_NAME=Font
'     PROP_VALUE=Segoe UI,9,400,0,0,0,1
'     PROP_NAME=ForeColor
'     PROP_VALUE=SYSTEM|WindowText
'     PROP_NAME=HideSelection
'     PROP_VALUE=True
'     PROP_NAME=Locked
'     PROP_VALUE=False
'     PROP_NAME=Multiline
'     PROP_VALUE=True
'     PROP_NAME=ReadOnly
'     PROP_VALUE=False
'     PROP_NAME=TextScrollBars
'     PROP_VALUE=ScrollBars.None
'     PROP_NAME=TabIndex
'     PROP_VALUE=2
'     PROP_NAME=TabStop
'     PROP_VALUE=True
'     PROP_NAME=Tag
'     PROP_VALUE=
'     PROP_NAME=ToolTip
'     PROP_VALUE=
'     PROP_NAME=ToolTipBalloon
'     PROP_VALUE=False
'     PROP_NAME=Text
'     PROP_VALUE=RichEdit1
'     PROP_NAME=TextAlign
'     PROP_VALUE=TextAlignment.Left
'     PROP_NAME=Visible
'     PROP_VALUE=True
'     PROP_NAME=WordWrap
'     PROP_VALUE=True
'   PROPERTIES_END
'   EVENTS_START
'   EVENTS_END
' WINFBE CONTROL_END
' WINFBE CONTROL_START Button
'   PROPERTIES_START
'     PROP_NAME=Name
'     PROP_VALUE=Button1
'     PROP_NAME=Left
'     PROP_VALUE=324
'     PROP_NAME=Top
'     PROP_VALUE=105
'     PROP_NAME=Width
'     PROP_VALUE=150
'     PROP_NAME=Height
'     PROP_VALUE=36
'     PROP_NAME=AllowDrop
'     PROP_VALUE=False
'     PROP_NAME=BackColor
'     PROP_VALUE=SYSTEM|Control
'     PROP_NAME=BackColorDown
'     PROP_VALUE=SYSTEM|Control
'     PROP_NAME=BackColorHot
'     PROP_VALUE=SYSTEM|Control
'     PROP_NAME=Font
'     PROP_VALUE=Segoe UI,9,400,0,0,0,1
'     PROP_NAME=TextForeColor
'     PROP_VALUE=SYSTEM|ControlText
'     PROP_NAME=TextBackColor
'     PROP_VALUE=SYSTEM|Control
'     PROP_NAME=TextForeColorDown
'     PROP_VALUE=SYSTEM|ControlText
'     PROP_NAME=TextBackColorDown
'     PROP_VALUE=SYSTEM|Control
'     PROP_NAME=Image
'     PROP_VALUE=
'     PROP_NAME=ImageWidth
'     PROP_VALUE=16
'     PROP_NAME=ImageHeight
'     PROP_VALUE=16
'     PROP_NAME=ImageMargin
'     PROP_VALUE=4
'     PROP_NAME=ImageHighDPI
'     PROP_VALUE=True
'     PROP_NAME=Text
'     PROP_VALUE=清除结果列表
'     PROP_NAME=TextAlign
'     PROP_VALUE=ButtonAlignment.MiddleCenter
'     PROP_NAME=TextMargin
'     PROP_VALUE=4
'     PROP_NAME=UseMnemonic
'     PROP_VALUE=True
'     PROP_NAME=ThemeSupport
'     PROP_VALUE=True
'     PROP_NAME=ToggleMode
'     PROP_VALUE=False
'     PROP_NAME=Enabled
'     PROP_VALUE=True
'     PROP_NAME=TabIndex
'     PROP_VALUE=3
'     PROP_NAME=TabStop
'     PROP_VALUE=True
'     PROP_NAME=Locked
'     PROP_VALUE=False
'     PROP_NAME=Tag
'     PROP_VALUE=
'     PROP_NAME=ToolTip
'     PROP_VALUE=
'     PROP_NAME=ToolTipBalloon
'     PROP_VALUE=False
'     PROP_NAME=Visible
'     PROP_VALUE=True
'   PROPERTIES_END
'   EVENTS_START
'   EVENTS_END
' WINFBE CONTROL_END
' WINFBE CONTROL_START Label
'   PROPERTIES_START
'     PROP_NAME=Name
'     PROP_VALUE=Label1
'     PROP_NAME=Left
'     PROP_VALUE=33
'     PROP_NAME=Top
'     PROP_VALUE=15
'     PROP_NAME=Width
'     PROP_VALUE=247
'     PROP_NAME=Height
'     PROP_VALUE=16
'     PROP_NAME=Text
'     PROP_VALUE=输入会员id，回车结束
'     PROP_NAME=AllowDrop
'     PROP_VALUE=False
'     PROP_NAME=BackColor
'     PROP_VALUE=SYSTEM|Control
'     PROP_NAME=BackColorHot
'     PROP_VALUE=SYSTEM|Control
'     PROP_NAME=ForeColor
'     PROP_VALUE=SYSTEM|ControlText
'     PROP_NAME=ForeColorHot
'     PROP_VALUE=SYSTEM|ControlText
'     PROP_NAME=BorderStyle
'     PROP_VALUE=ControlBorderStyle.None
'     PROP_NAME=TextAlign
'     PROP_VALUE=LabelAlignment.TopLeft
'     PROP_NAME=Font
'     PROP_VALUE=Segoe UI,9,400,0,0,0,1
'     PROP_NAME=Enabled
'     PROP_VALUE=True
'     PROP_NAME=Tag
'     PROP_VALUE=
'     PROP_NAME=ToolTip
'     PROP_VALUE=
'     PROP_NAME=ToolTipBalloon
'     PROP_VALUE=False
'     PROP_NAME=Locked
'     PROP_VALUE=False
'     PROP_NAME=UseMnemonic
'     PROP_VALUE=True
'     PROP_NAME=Visible
'     PROP_VALUE=True
'   PROPERTIES_END
'   EVENTS_START
'   EVENTS_END
' WINFBE CONTROL_END
' WINFBE CONTROL_START Label
'   PROPERTIES_START
'     PROP_NAME=Name
'     PROP_VALUE=Label2
'     PROP_NAME=Left
'     PROP_VALUE=33
'     PROP_NAME=Top
'     PROP_VALUE=125
'     PROP_NAME=Width
'     PROP_VALUE=88
'     PROP_NAME=Height
'     PROP_VALUE=16
'     PROP_NAME=Text
'     PROP_VALUE=结果列表
'     PROP_NAME=AllowDrop
'     PROP_VALUE=False
'     PROP_NAME=BackColor
'     PROP_VALUE=SYSTEM|Control
'     PROP_NAME=BackColorHot
'     PROP_VALUE=SYSTEM|Control
'     PROP_NAME=ForeColor
'     PROP_VALUE=SYSTEM|ControlText
'     PROP_NAME=ForeColorHot
'     PROP_VALUE=SYSTEM|ControlText
'     PROP_NAME=BorderStyle
'     PROP_VALUE=ControlBorderStyle.None
'     PROP_NAME=TextAlign
'     PROP_VALUE=LabelAlignment.TopLeft
'     PROP_NAME=Font
'     PROP_VALUE=Segoe UI,9,400,0,0,0,1
'     PROP_NAME=Enabled
'     PROP_VALUE=True
'     PROP_NAME=Tag
'     PROP_VALUE=
'     PROP_NAME=ToolTip
'     PROP_VALUE=
'     PROP_NAME=ToolTipBalloon
'     PROP_VALUE=False
'     PROP_NAME=Locked
'     PROP_VALUE=False
'     PROP_NAME=UseMnemonic
'     PROP_VALUE=True
'     PROP_NAME=Visible
'     PROP_VALUE=True
'   PROPERTIES_END
'   EVENTS_START
'   EVENTS_END
' WINFBE CONTROL_END
' WINFBE FORM_END
' WINFBE_CODEGEN_START
#if 0
type frmMainType extends wfxForm
    private:
        temp as byte
    public:
        declare static function FormInitializeComponent( byval pForm as frmMainType ptr ) as LRESULT
        declare constructor
        ' Controls
        Text1 As wfxTextBox
        RichEdit1 As wfxRichEdit
        Button1 As wfxButton
        Label1 As wfxLabel
        Label2 As wfxLabel
end type


function frmMainType.FormInitializeComponent( byval pForm as frmMainType ptr ) as LRESULT
    dim as long nClientOffset

    pForm->Name = "frmMain"
    pForm->Text = "form1"
    pForm->StartPosition = FormStartPosition.CenterScreen
    pForm->SetBounds(10,10,527,431)
    pForm->Text1.Parent = pForm
    pForm->Text1.Name = "Text1"
    pForm->Text1.Text = "Text1"
    pForm->Text1.SetBounds(33,39-nClientOffset,454,36)
    pForm->RichEdit1.Parent = pForm
    pForm->RichEdit1.Name = "RichEdit1"
    pForm->RichEdit1.Text = "RichEdit1"
    pForm->RichEdit1.SetBounds(33,150-nClientOffset,449,234)
    pForm->Button1.Parent = pForm
    pForm->Button1.Name = "Button1"
    pForm->Button1.Text = "清除结果列表"
    pForm->Button1.SetBounds(324,105-nClientOffset,150,36)
    pForm->Label1.Parent = pForm
    pForm->Label1.Name = "Label1"
    pForm->Label1.Text = "输入会员id，回车结束"
    pForm->Label1.SetBounds(33,15-nClientOffset,247,16)
    pForm->Label2.Parent = pForm
    pForm->Label2.Name = "Label2"
    pForm->Label2.Text = "结果列表"
    pForm->Label2.SetBounds(33,125-nClientOffset,88,16)
    pForm->Controls.Add(ControlType.TextBox, @(pForm->Text1))
    pForm->Controls.Add(ControlType.RichEdit, @(pForm->RichEdit1))
    pForm->Controls.Add(ControlType.Button, @(pForm->Button1))
    pForm->Controls.Add(ControlType.Label, @(pForm->Label1))
    pForm->Controls.Add(ControlType.Label, @(pForm->Label2))
    Application.Forms.Add(ControlType.Form, pForm)
    function = 0
end function

constructor frmMainType
    InitializeComponent = cast( any ptr, @FormInitializeComponent )
    this.FormInitializeComponent( @this )
end constructor

dim shared frmMain as frmMainType
#endif
' WINFBE_CODEGEN_END
' frmMain form code file

Function frmMain_Load( ByRef sender As wfxForm, ByRef e As EventArgs ) As LRESULT
    frmMain.Text = ""
frmMain.Text1.Text = ""
frmMain.RichEdit1.Text = ""
Function = 0
end Function
