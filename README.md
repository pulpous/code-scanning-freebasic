# FreeBasic

目前没有写代码:

官网 - https://www.freebasic.net

文档 - https://www.freebasic.net/wiki/DocToc

支持 windows/Linux 平台

# 下载
- [fbc - freeBasic](https://sourceforge.net/projects/fbc/files/FreeBASIC-1.08.1/Binaries-Windows/FreeBASIC-1.08.1-win64.zip/download) `FreeBASIC-1.08.1-win64.zip`
- [我用的编辑器 - winFBE(`WinFBE Editor and Visual Designer`)](https://github.com/PaulSquires/WinFBE/releases) `winfbe_suite_setup.exe`

# 打开项目
`main.wfbe` -> `右键`/`打开方式` -> `winfbe`

# 编辑器 - 还有那些
[官网说明](https://www.freebasic.net/wiki/CompilerInstalling)

> 官方推荐
- [FBIDE](http://fbide.freebasic.net/)
- [FBEdit](http://radasm.cherrytree.at/fbedit/)
- [WinFBE Editor and Visual Designer](https://www.freebasic.net/forum/viewtopic.php?f=8&t=25215) 不支持中文,用就崩溃 (utf-16)

> 我自己在论坛中找到的
- [VisualFBEditor](https://www.freebasic.net/forum/viewtopic.php?f=8&t=27284) [GitHub - VisualFBEditor](https://github.com/XusinboyBekchanov/VisualFBEditor)
- [poseidonFB](https://www.freebasic.net/forum/viewtopic.php?f=8&t=23935) 日本
- [VisualFreeBasic](https://www.freebasic.net/forum/viewtopic.php?f=8&t=28522) &nbsp; &nbsp;[GitHub](https://github.com/xiaoyaocode163/VisualFreeBasic) &nbsp;&nbsp; [VisualFreeBasic 5.7.0](http://www.yfvb.com/soft-48.htm) 不开源